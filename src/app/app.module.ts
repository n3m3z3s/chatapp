import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';

var firebaseConfig = { 
  apiKey: "AIzaSyBkDsFy7pRQnUdKYvgrbcq-r2D7B13RFaI",
  authDomain: "chatzap-46c15.firebaseapp.com",
  databaseURL: "https://chatzap-46c15.firebaseio.com",
  projectId: "chatzap-46c15",
  storageBucket: "chatzap-46c15.appspot.com",
  messagingSenderId: "555228739783",
  appId: "1:555228739783:web:f4a3a205a04bae67e926e2",
  measurementId: "G-T5LVM37C2H"
};

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],

  imports: [BrowserModule, IonicModule.forRoot(),  AngularFireAuthModule, AngularFirestoreModule, AppRoutingModule,AngularFireModule.initializeApp(firebaseConfig)],
 
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
